import { Component } from '@angular/core';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  key = [
    'rumah',
    'mobil',
    'motor',
    'samsung s20',
    'playstation 5',
    'gelas cantik',
    'piring',
    'telepon',
    'toples',
    'kursi hangat',
    'kayu bakar'
  ];

  constructor() {}

}
