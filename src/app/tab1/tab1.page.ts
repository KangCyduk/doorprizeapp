import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalPage } from './../shared/modal/modal.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  key = [
    'juara 1',
    'juara 2',
    'juara 3'
  ];

  valuelist = [
    'pemenang'
  ];

  constructor(
    private modalCtrl: ModalController
  ) {}

  async showmodal(act: string){
    const modal =  await this.modalCtrl.create({
      component: ModalPage,
      cssClass: 'cssmodal',
      componentProps: {
        action: act
      }
    });
    return modal.present();
  }
}
